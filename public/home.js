var room_no = "room1";

$("#sign-out").click(function() {
    var sout = confirm("You're signing out from the forum.");
    if (sout == true) {
        firebase.auth().signOut().then(function() {
            alert("Successfully signed out.");
            window.location.href="index.html";
        }).catch(function(error) {
            code = error.code;
            msg = error.message;
            alert("Error " + code + "\n" + msg);
        })
    };
});
$("button[type=submit]").click(function() {
    var post = $("textarea").val();
    var user = firebase.auth().currentUser;
    if (user == null) {
        alert("Please sign in first.");
        window.location.href="index.html";
    }
    var uemail = user.email;
    if (post != "") {
        updateCommentToDb(uemail, post);
        fetchCommentFromDb();
        $("textarea").val('');
    }
    else alert("Are you sure you're not gonna say something?");
    return false;
});
$("#room1").click(function() {
    if (room_no != "room1") {
        $("#"+room_no).toggleClass("active")
        room_no = "room1";
        $("#"+room_no).toggleClass("active")
        fetchCommentFromDb();
    }
});
$("#room2").click(function() {
    if (room_no != "room2") {
        $("#"+room_no).toggleClass("active")
        room_no = "room2";
        $("#"+room_no).toggleClass("active")
        fetchCommentFromDb();
    }
});
$("#room3").click(function() {
    if (room_no != "room3") {
        $("#"+room_no).toggleClass("active")
        room_no = "room3";
        $("#"+room_no).toggleClass("active")
        fetchCommentFromDb();
    }
});
function updateCommentToDb(email, comment) {
    firebase.database().ref(room_no).push().set({
      email: email,
      comment: comment
    });
}
function fetchCommentFromDb() {
    var before_email = "<div class='text-dark pt-3'><p class='pb-3 mb-0 border-bottom border-gray'><strong class='email'>";
    var middle = "</strong> says:<br><a class='comments pl-5'>";
    var after_cmt = "</a></p></div>";
    var html_code = [];

    firebase.database().ref(room_no).once('value').then(function(snapshot) {
        snapshot.forEach(data => {
            var cmt = data.val();
            html_code.push(before_email + cmt.email + middle + cmt.comment + after_cmt);
        });
        $("#history").html(html_code.join(''));
    })
}
$(window).ready(function() {
    fetchCommentFromDb();
});