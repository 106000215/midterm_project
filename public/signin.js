$("#sign-in").click(function() {
    var email = $("input[type=email]").val();
    var pwd = $("input[type=password]").val();
    firebase.auth().signInWithEmailAndPassword(email, pwd).then(function() {
        alert("success");
        window.location.href="home.html";
    }).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        alert("Error "+ errorCode + ":\n" + errorMessage + "\nPlease try again.");
    });
});
$("#google-sign-in").click(function() {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider).then(function(result) {
        // var token = result.credential.accessToken;
        // var user = result.user;
        alert("login success");
        window.location.href='home.html';
    }).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        alert("Error " + errorCode + ":\n" + errorMessage + "\nPlease try again.");
    });
});
$("#sign-up").click(function() {
    var email = $("input[type=email]").val();
    var pwd = $("input[type=password]").val();
    firebase.auth().createUserWithEmailAndPassword(email, pwd).catch(function(error) {
        var errorCode = error.code;
        var errorMessage = error.message;
        alert("Error "+ errorCode + ":\n" + errorMessage + "\nPlease try again.");
      });
})