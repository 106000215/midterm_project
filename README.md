# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : Normal Group Chat
* Key functions (add/delete)
    1. add msg
    2. switch group
    
* Other functions (add/delete)
    1. Third party sign-in

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|

## Website Detail Description
+ This is a simple chat group webpage, where there are 3 groups for user to join. Messages of each 
group are seperated.

# [作品網址](https://my-chatroom-cd6e0.firebaseapp.com/)